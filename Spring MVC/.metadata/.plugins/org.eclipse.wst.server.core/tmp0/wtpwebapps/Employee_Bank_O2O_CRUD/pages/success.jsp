<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="2">
<tr>
<th>empId</th>
<th>empName</th>
<th>empPost</th>
<th>empMobNo</th>
<th>username</th>
<th>password</th>
<th>accNo</th>
<th>bankName</th>
<th>branchName</th>
<th>ifscCode</th>
</tr>

<c:forEach items="${list}" var="emp">
<tr>
<td>${emp.empId}</td>
<td>${emp.empName}</td>
<td>${emp.empPost}</td>
<td>${emp.empMobNo}</td>
<td>${emp.username}</td>
<td>${emp.password}</td>
<td>${emp.bank.accNo}</td>
<td>${emp.bank.bankName}</td>
<td>${emp.bank.branchName}</td>
<td>${emp.bank.ifscCode}</td>
<td><a href="delete?empId=${emp.empId}accNo=${emp.bank.accNo}">DELETE</a>||<a href="edit?empId=${emp.empId}accNo=${emp.bank.accNo}">EDIT</a></td>
</tr>
</c:forEach>

</table>
</body>
</html>