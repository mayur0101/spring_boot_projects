<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="2">
<tr>
<th>EID</th>
<th>Ename</th>
<th>UserName</th>
<th>Password</th>
<th>Operation</th>
</tr>

<c:forEach items="${list}" var="e">
<tr>
<td>${e.eid}</td>
<td>${e.ename}</td>
<td>${e.uname}</td>
<td>${e.pass}</td>
<td><a href="delete?eid=${e.eid}">DELETE</a>||<a href="edit?eid=${e.eid}">EDIT</a></td>
</tr>
</c:forEach>

</table>
</body>
</html>