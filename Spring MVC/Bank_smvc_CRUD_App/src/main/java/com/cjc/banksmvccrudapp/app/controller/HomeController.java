package com.cjc.banksmvccrudapp.app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cjc.banksmvccrudapp.app.model.Bank;

@Controller
public class HomeController {
	

	
	@RequestMapping("/")
	public String preLogin()
	{
		return "login";
	}
	
	@RequestMapping("/openRegpage")
	public String preRegister()
	{
		return "register";
	}
	
	@RequestMapping("/save")
	public String saveStudent(@ModelAttribute Bank b)
	{
		System.out.println(b.getAccNo());
		System.out.println(b.getAccHolderName());
		System.out.println(b.getAccHolderAge());
		System.out.println(b.getAccBalance());
		System.out.println(b.getAccType());
		return "register";
	}


}
