package com.cjc.banksmvccrudapp.app.model;


public class Bank {
	
	private int accNo;
	private String accType;
	private String accHolderName;
	private int accHolderAge;
	private float accBalance;
	
	public int getAccNo() {
		return accNo;
	}
	public void setAccNo(int accNo) {
		System.out.println("acc No");
		this.accNo = accNo;
	}
	public String getAccType() {
		return accType;
	}
	public void setAccType(String accType) {
		this.accType = accType;
	}
	public String getAccHolderName() {
		return accHolderName;
	}
	public void setAccHolderName(String accHolderName) {
		this.accHolderName = accHolderName;
	}
	public int getAccHolderAge() {
		return accHolderAge;
	}
	public void setAccHolderAge(int accHolderAge) {
		this.accHolderAge = accHolderAge;
	}
	public float getAccBalance() {
		return accBalance;
	}
	public void setAccBalance(float accBalance) {
		this.accBalance = accBalance;
	}

}
