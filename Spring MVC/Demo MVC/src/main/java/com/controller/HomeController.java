package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class HomeController {
	
	@RequestMapping("/log")
	public String preLogin()
	{
		return "login";
	}
	
	@RequestMapping("/acc")
	public String preAccount()
	{
		return "account";
	}

}
