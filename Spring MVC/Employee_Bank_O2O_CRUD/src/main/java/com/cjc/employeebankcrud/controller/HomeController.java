package com.cjc.employeebankcrud.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cjc.employeebankcrud.model.Employee;
import com.cjc.employeebankcrud.servicei.ServiceI;

@Controller
public class HomeController {
	
	@Autowired
	ServiceI esi;
	
	@RequestMapping("/")
	public String preLogin()
	{
		return "login";
	}
	
	@RequestMapping("/openRegpage")
	public String preRegister()
	{
		return "register";
	}
	
	@RequestMapping("/save")
	public String saveEmployee(@ModelAttribute Employee emp)
	{
		esi.saveEmployee(emp);
		return "register";
	}
	
	@RequestMapping("/login")
	public String loginEmployee(@RequestParam String username, @RequestParam String password, Model m)
	{
		List<Employee> list = esi.loginEmployee(username, password);
		if(!list.isEmpty())
		{
			m.addAttribute("list", list);
			return "success";
		}
		else
		{
			return "login";
		}
	}
	
	@RequestMapping("/delete")
	public String deleteEmployee(@RequestParam int empId, @RequestParam int accNo, Model m)
	{
		System.out.println(empId);
		List<Employee> list = esi.deleteEmployee(empId, accNo);
		m.addAttribute("list", list);
		return "success";
	}
	
}
