package com.cjc.employeebankcrud.daoi;

import java.util.List;

import com.cjc.employeebankcrud.model.Employee;

public interface DaoI {

	void saveEmployee(Employee emp);

	List<Employee> loginEmployee(String username, String password);

	List<Employee> deleteEmployee(int empId, int accNo);

}
