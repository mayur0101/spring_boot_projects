package com.cjc.employeebankcrud.daoimpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cjc.employeebankcrud.daoi.DaoI;
import com.cjc.employeebankcrud.model.Bank;
import com.cjc.employeebankcrud.model.Employee;


@Repository
public class DaoImpl implements DaoI {
	
	@Autowired
	SessionFactory sf;

	@Override
	public void saveEmployee(Employee emp) {
		
		Session sess = sf.openSession();
		sess.save(emp);
		sess.beginTransaction().commit();
		
	}
	
	public List<Employee> getAllEmployee()
	{
		Session sess = sf.openSession();
		List<Employee> list = sess.createQuery("from Employee").getResultList();
		
		return list;
	}

	@Override
	public List<Employee> loginEmployee(String username, String password) {
		
		if(username.equals("ADMIN") && password.equals("ADMIN"))
		{
			return getAllEmployee();
		}
		else
		{
			Session sess = sf.openSession();
			Query<Employee> q = sess.createQuery("from Employee where username = ? and password = ?");
				q.setParameter(0, username);
				q.setParameter(1, password);
				List<Employee> list = q.getResultList();
			
			return list;	
		}
		
		
	}

	@Override
	public List<Employee> deleteEmployee(int empId, int accNo) {
		
//		System.out.println("si empId");
		
		deleteBank(accNo);
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Query<Employee> q = sess.createQuery("delete from Employee where empId = ?");
			q.setParameter(0, empId);
			q.executeUpdate();
		tx.commit();
		
		return getAllEmployee();
	}
	
	public void deleteBank(int accNo)
	{
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		Query<Bank> q = sess.createQuery("delete from Bank where accNo = ?");
			q.setParameter(0, accNo);
			q.executeUpdate();
		tx.commit();
	}

}
