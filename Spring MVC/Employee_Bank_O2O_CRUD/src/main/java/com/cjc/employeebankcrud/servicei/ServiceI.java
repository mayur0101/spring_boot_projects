package com.cjc.employeebankcrud.servicei;

import java.util.List;

import com.cjc.employeebankcrud.model.Employee;

public interface ServiceI {

	void saveEmployee(Employee emp);
	List<Employee> loginEmployee(String username, String password);
	List<Employee> deleteEmployee(int empId, int accNo);

}
