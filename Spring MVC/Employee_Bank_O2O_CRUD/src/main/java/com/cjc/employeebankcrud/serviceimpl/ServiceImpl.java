package com.cjc.employeebankcrud.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cjc.employeebankcrud.daoi.DaoI;
import com.cjc.employeebankcrud.model.Employee;
import com.cjc.employeebankcrud.servicei.ServiceI;


@Service
public class ServiceImpl implements ServiceI {
	
	@Autowired
	DaoI edi;

	@Override
	public void saveEmployee(Employee emp) {
		
		edi.saveEmployee(emp);
		
	}

	@Override
	public List<Employee> loginEmployee(String username, String password) {
		
		return edi.loginEmployee(username, password);
	}

	@Override
	public List<Employee> deleteEmployee(int empId, int accNo) {
		
		System.out.println("ssi empId");
		
		return edi.deleteEmployee(empId, accNo);
	}

}
