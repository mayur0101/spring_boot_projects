package com.cjc.employeemvccrud.app.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cjc.employeemvccrud.app.model.Employee;
import com.cjc.employeemvccrud.app.service.HomeService;



@Controller
public class HomeController {
	
	@Autowired
	HomeService hs;
	
	@RequestMapping("/")
	public String preLogin()
	{
		return "login";
	}
	
	@RequestMapping("/openRegpage")
	public String preRegister()
	{
		return "register";
	}
	
	@RequestMapping("/save")
	public String saveData(@ModelAttribute Employee e)
	{
		hs.saveData(e);
		return "login";
	}
	
	@RequestMapping("/login")
	public String loginEmployee(@RequestParam String uname, @RequestParam String pass, Model m) 
	{
		List<Employee> list = hs.loginEmployee(uname, pass);
		
		if(!list.isEmpty())
		{
			m.addAttribute("list", list);
			return "success";
		}
		else
		{
			return "login";
		}
	}
	
	@RequestMapping("/delete")
	public String deleteStudent(@RequestParam int eid, Model m)
	{
		List<Employee> list = hs.deleteEmployee(eid);
		m.addAttribute("list", list);
		return "success"; 
	}
	
}
