package com.cjc.employeemvccrud.app.dao;

import java.util.List;

import com.cjc.employeemvccrud.app.model.Employee;

public interface HomeDao {

	void saveData(Employee e);

	List<Employee> loginEmployee(String uname, String pass);

	List<Employee> deleteEmployee(int eid);

}
