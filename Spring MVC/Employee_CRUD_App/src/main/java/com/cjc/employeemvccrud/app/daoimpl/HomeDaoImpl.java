package com.cjc.employeemvccrud.app.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cjc.employeemvccrud.app.dao.HomeDao;
import com.cjc.employeemvccrud.app.model.Employee;


@Repository
public class HomeDaoImpl implements HomeDao {
	
	@Autowired
	SessionFactory sf;

	@Override
	public void saveData(Employee e) {
		
		Session sess = sf.openSession();
		sess.save(e);
		sess.beginTransaction().commit();
		
	}
	
	public List<Employee> getAllEmployee()
	{
		Session sess = sf.openSession();
		List<Employee> list = sess.createQuery("from Employee").getResultList();
		
		sess.beginTransaction().commit();
		
		return list;
	}

	@Override
	public List<Employee> loginEmployee(String uname, String pass) {
		
		if(uname.equals("ADMIN") && pass.equals("ADMIN"))
		{
			return getAllEmployee();
		}
		else 
		{
			Session sess = sf.openSession();
			
			Query<Employee> q = sess.createQuery("from Employee where uname = ? and pass = ?");
						   q.setParameter(0, uname);
						   q.setParameter(1, pass);
			List<Employee> list = q.getResultList();
			
			return list;
		}
		
	}

	@Override
	public List<Employee> deleteEmployee(int eid) {
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		
		Query<Employee> q = sess.createQuery("delete from Employee where eid = ?");
					   q.setParameter(0, eid);
					   q.executeUpdate();
					   
		return getAllEmployee();
	}

}
