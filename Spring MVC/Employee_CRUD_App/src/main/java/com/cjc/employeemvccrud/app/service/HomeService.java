package com.cjc.employeemvccrud.app.service;

import java.util.List;

import com.cjc.employeemvccrud.app.model.Employee;

public interface HomeService {

	void saveData(Employee e);

	List<Employee> loginEmployee(String uname, String pass);

	List<Employee> deleteEmployee(int eid);

}
