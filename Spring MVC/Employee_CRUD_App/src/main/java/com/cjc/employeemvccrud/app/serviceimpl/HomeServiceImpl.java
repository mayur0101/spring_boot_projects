package com.cjc.employeemvccrud.app.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cjc.employeemvccrud.app.dao.HomeDao;
import com.cjc.employeemvccrud.app.model.Employee;
import com.cjc.employeemvccrud.app.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService {
	
	@Autowired
	HomeDao hd;

	@Override
	public void saveData(Employee e) {
		
		hd.saveData(e);
		
	}

	@Override
	public List<Employee> loginEmployee(String uname, String pass) {
		
		return hd.loginEmployee(uname, pass);
	}

	@Override
	public List<Employee> deleteEmployee(int eid) {
		
		return hd.deleteEmployee(eid);
	}
	
	

	
	
}
