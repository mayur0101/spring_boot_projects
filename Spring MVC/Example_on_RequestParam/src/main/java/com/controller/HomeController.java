package com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HomeController {
	
//	@RequestMapping("/login")
//	public String loginPage()
//	{
//		return "login";
//	}
	
	@RequestMapping("/login")
	public String display(@RequestParam("username") String uname, @RequestParam("password") String pass, Model m) {
		m.addAttribute("u", uname);
		m.addAttribute("p", pass);
		return "login";
	}

}
