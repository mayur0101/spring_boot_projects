package com.cjc.springmvccrudapp.app.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cjc.springmvccrudapp.app.model.Student;
import com.cjc.springmvccrudapp.app.servicei.ServiceI;

@Controller
public class HomeController {	
	
	@Autowired
	ServiceI ssi;
	
	@RequestMapping("/")
	public String preLogin()
	{
		
		return "login";
	}
	
	@RequestMapping("/openRegpage")
	public String preRegister()
	{
		return "register";
	}
	
	@RequestMapping("/save")
	public String saveStudent (@ModelAttribute Student s)
	{		
		ssi.saveStudent(s);
		
		return "login";
	}
	
	@RequestMapping("/login")
	public String loginStudent(@RequestParam String username, @RequestParam String password, Model m) 
	{
		List<Student> list = ssi.loginStudent(username, password);
		
		if(!list.isEmpty())
		{
			m.addAttribute("list", list);
			return "success";
		}
		else
		{
			return "login";
		}
	}
	
	@RequestMapping("/delete")
	public String deleteStudent(@RequestParam int sid, Model m)
	{
		List<Student> list = ssi.deleteStudent(sid);
		m.addAttribute("list", list);
		return "success"; 
	}
	
	@RequestMapping("/edit")
	public String editStudent(@RequestParam int sid, Model m)
	{
		Student s = ssi.editStudent(sid);
		m.addAttribute("stu", s);
		return "edit";
	}
	
	@RequestMapping("/update")
	public String updateStudent(@ModelAttribute Student s)
	{
		ssi.updateStudent(s);
		return "login";
	}

}
