package com.cjc.springmvccrudapp.app.daoi;

import java.util.List;

import com.cjc.springmvccrudapp.app.model.Student;

public interface DaoI {
	
	public void saveStudent(Student s);
	public List<Student> loginStudent(String username, String password);
	public List<Student> deleteStudent(int sid);
	public Student editStudent(int sid);
	public void updateStudent(Student s);
	
}
