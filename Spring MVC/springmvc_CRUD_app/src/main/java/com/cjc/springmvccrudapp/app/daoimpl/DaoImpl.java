package com.cjc.springmvccrudapp.app.daoimpl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.cjc.springmvccrudapp.app.daoi.DaoI;
import com.cjc.springmvccrudapp.app.model.Student;

@Repository
public class DaoImpl implements DaoI {
	
	@Autowired
	SessionFactory sf;
	
	public List<Student> getAllStudent()
	{
		Session sess = sf.openSession();
		List<Student> list = sess.createQuery("from Student").getResultList();
		
		sess.beginTransaction().commit();
		
		return list;
	}

	@Override
	public void saveStudent(Student s) {
	
		Session sess = sf.openSession();
		sess.save(s);
		sess.beginTransaction().commit();
		
	}

	@Override
	public List<Student> loginStudent(String username, String password) {
		
		if(username.equals("ADMIN") && password.equals("ADMIN"))
		{
			return getAllStudent();
		}
		else 
		{
			Session sess = sf.openSession();
			
			Query<Student> q = sess.createQuery("from Student where username = ? and password = ?");
						   q.setParameter(0, username);
						   q.setParameter(1, password);
			List<Student> list = q.getResultList();
			
			return list;
		}
		
	}

	@Override
	public List<Student> deleteStudent(int sid) {
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		
		Query<Student> q = sess.createQuery("delete from Student where sid = ?");
					   q.setParameter(0, sid);
					   q.executeUpdate();
					   
		return getAllStudent();
	}

	@Override
	public Student editStudent(int sid) {
		
		Session sess = sf.openSession();
		Student s = sess.get(Student.class, sid);
		
		return s;
	}

	@Override
	public void updateStudent(Student s) {
		
		Session sess = sf.openSession();
		Transaction tx = sess.beginTransaction();
		sess.update(s);
		tx.commit();
		
	}

}
