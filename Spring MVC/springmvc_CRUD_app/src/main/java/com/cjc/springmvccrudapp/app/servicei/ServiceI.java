package com.cjc.springmvccrudapp.app.servicei;

import java.util.List;

import org.springframework.web.bind.annotation.RequestParam;

import com.cjc.springmvccrudapp.app.model.Student;

public interface ServiceI {
	
	public void saveStudent(Student s);
	public List<Student> loginStudent(String username, String password);
	public List<Student> deleteStudent(int sid);
	public Student editStudent(int sid);
	public void updateStudent(Student s);
	
}
