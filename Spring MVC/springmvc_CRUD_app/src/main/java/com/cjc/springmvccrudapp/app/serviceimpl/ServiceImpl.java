package com.cjc.springmvccrudapp.app.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cjc.springmvccrudapp.app.daoi.DaoI;
import com.cjc.springmvccrudapp.app.model.Student;
import com.cjc.springmvccrudapp.app.servicei.ServiceI;

@Service
public class ServiceImpl implements ServiceI {
	
	@Autowired
	DaoI sdi;

	@Override
	public void saveStudent(Student s) {
		
		System.out.println("ServiceImpl SaveStudent");
		sdi.saveStudent(s);
		
	}

	@Override
	public List<Student> loginStudent(String username, String password) {
		
		List<Student> list = sdi.loginStudent(username, password);		
		return list;
	}

	@Override
	public List<Student> deleteStudent(int sid) {
		
		return sdi.deleteStudent(sid);
	}

	@Override
	public Student editStudent(int sid) {
		
		return sdi.editStudent(sid);
	}

	@Override
	public void updateStudent(Student s) {
		
		sdi.updateStudent(s);
		
	}

}
