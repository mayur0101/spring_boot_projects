<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<table border="2">
<tr>
<th>SID</th>
<th>Sname</th>
<th>UserName</th>
<th>Password</th>
<th>Operation</th>
</tr>

<c:forEach items="${list}" var="s">
<tr>
<td>${s.sid}</td>
<td>${s.sname}</td>
<td>${s.username}</td>
<td>${s.password}</td>
<td><a href="delete?sid=${s.sid}">DELETE</a>||<a href="edit?sid=${s.sid}">EDIT</a></td>
</tr>
</c:forEach>

</table>
</body>
</html>