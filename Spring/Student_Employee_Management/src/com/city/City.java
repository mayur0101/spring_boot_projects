package com.city;

public class City {

	private int cid;
	private String cname;
	private int cpin;
	
	public int getCid() {
		return cid;
	}
	public void setCid(int cid) {
		this.cid = cid;
	}
	public String getCname() {
		return cname;
	}
	public void setCname(String cname) {
		this.cname = cname;
	}
	public int getCpin() {
		return cpin;
	}
	public void setCpin(int cpin) {
		this.cpin = cpin;
	}
	
}
