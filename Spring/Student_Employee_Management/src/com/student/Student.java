package com.student;

import com.city.City;

public class Student {
	
	private int sid;
	private String sname;
	private long smobNo;
	private City scity;
	
	public int getSid() {
		return sid;
	}
	public void setSid(int sid) {
		this.sid = sid;
	}
	public String getSname() {
		return sname;
	}
	public void setSname(String sname) {
		this.sname = sname;
	}
	public long getSmobNo() {
		return smobNo;
	}
	public void setSmobNo(long smobNo) {
		this.smobNo = smobNo;
	}
	public City getScity() {
		return scity;
	}
	public void setScity(City scity) {
		this.scity = scity;
	}

}
